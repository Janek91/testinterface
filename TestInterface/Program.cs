﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TestInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            StoreAInfo a = ReadEmbeddedFile(Properties.Resources.StoreA).FromJson<StoreAInfo>();
            StoreBInfo b = ReadEmbeddedFile(Properties.Resources.StoreB).FromJson<StoreBInfo>();

            List<IStoreInfo<IStoreExtraInfo<StoreInfo>, StoreInfo>> storesList = new List<IStoreInfo<IStoreExtraInfo<StoreInfo>, StoreInfo>>
            {
               (IStoreInfo<IStoreExtraInfo<StoreInfo>, StoreInfo>) a,
                (IStoreInfo<IStoreExtraInfo<StoreInfo>, StoreInfo>)b
            };

            Console.Write(string.Join(Environment.NewLine, storesList.Select(x => x.StoresString)));
            Console.ReadLine();
        }

        private static string ReadEmbeddedFile(byte[] fileBytes)
        {
            using (Stream stream = new MemoryStream(fileBytes))
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
