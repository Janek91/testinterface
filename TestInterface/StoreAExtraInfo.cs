﻿namespace TestInterface
{
    public class StoreAExtraInfo<TStore> : StoreExtraInfo<TStore>
        where TStore : StoreInfo
    {
        public string StoreAExtraString { get; set; }
    }
}
