﻿namespace TestInterface
{
    public class StoreB<TStore> : StoreBase<StoreBExtraInfo<TStore>, TStore>
        where TStore : Store
    {
        public TStore[] BStores { get; set; }
    }
}
