﻿namespace TestInterface
{
    public interface IStoreInfo<T, TVideo> : IStore<T, TVideo>
        where T : IStoreExtraInfo<TVideo>
        where TVideo : StoreInfo
    {
        string StoresString { get; }
    }
}
