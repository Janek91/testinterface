﻿using System;
using System.Linq;

namespace TestInterface
{
    public class StoreBInfo : StoreB<StoreInfo>, IStoreInfo<StoreBExtraInfo<StoreInfo>, StoreInfo>
    {
        public string StoresString => string.Join(Environment.NewLine, BStores.Select(x => $"{x.City} {x.Name}"));
    }
}
