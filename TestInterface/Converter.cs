﻿using Newtonsoft.Json;
using System.IO;

namespace TestInterface
{
    public static class Converter
    {
        public static JsonSerializerSettings DefaultSettings { get; set; } = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            NullValueHandling = NullValueHandling.Ignore,
            Formatting = Formatting.None
        };

        public static string ToJson(this object self, JsonSerializerSettings settings = null, Formatting formatting = Formatting.None)
        {
            return JsonConvert.SerializeObject(self, formatting, settings ?? DefaultSettings);
        }

        public static T FromJson<T>(this string json, JsonSerializerSettings settings = null)
        {
            return JsonConvert.DeserializeObject<T>(json, settings ?? DefaultSettings);
        }

        public static object FromJson(this string json, JsonSerializerSettings settings = null)
        {
            return JsonConvert.DeserializeObject(json, settings ?? DefaultSettings);
        }

        public static object DeserializeFromStream(this Stream stream, JsonSerializerSettings settings = null)
        {
            JsonSerializer serializer = JsonSerializer.Create(settings ?? DefaultSettings);
            return stream.DeserializeFromStream(serializer);
        }

        public static object DeserializeFromStream(this Stream stream, JsonSerializer serializer = null)
        {
            if (serializer == null)
            {
                serializer = JsonSerializer.Create(DefaultSettings);
            }

            using (StreamReader sr = new StreamReader(stream))
            using (JsonTextReader jsonTextReader = new JsonTextReader(sr))
            {
                return serializer.Deserialize(jsonTextReader);
            }
        }

        public static T DeserializeFromStreamWithSettings<T>(this Stream stream, JsonSerializerSettings settings = null)
        {
            JsonSerializer serializer = JsonSerializer.Create(settings ?? DefaultSettings);
            return stream.DeserializeFromStream<T>(serializer);
        }

        public static T DeserializeFromStream<T>(this Stream stream, JsonSerializer serializer = null)
        {
            if (serializer == null)
            {
                serializer = JsonSerializer.Create(DefaultSettings);
            }

            using (StreamReader sr = new StreamReader(stream))
            using (JsonTextReader jsonTextReader = new JsonTextReader(sr))
            {
                return serializer.Deserialize<T>(jsonTextReader);
            }
        }
    }
}
