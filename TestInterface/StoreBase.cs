﻿using System.Collections.Generic;

namespace TestInterface
{
    public class StoreBase<T, TStore> : IStore<T, TStore>
        where T : IStoreExtraInfo<TStore>
        where TStore : Store
    {
        public T Extra { get; set; }
        public IList<TStore> StoresInfo => Extra.Stores;
    }
}
