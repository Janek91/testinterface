﻿using System.Collections.Generic;

namespace TestInterface
{
    public class StoreExtraInfo<TStore> : IStoreExtraInfo<TStore>
      where TStore : Store
    {
        public IList<TStore> Stores { get; set; }
    }
}
