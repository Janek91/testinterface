﻿using System.Collections.Generic;

namespace TestInterface
{
    public interface IStore<T, TStore>
        where T : IStoreExtraInfo<TStore>
        where TStore : Store
    {
        T Extra { get; set; }
        IList<TStore> StoresInfo { get; }
    }
}
