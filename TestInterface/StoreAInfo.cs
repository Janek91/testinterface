﻿using System;
using System.Linq;

namespace TestInterface
{
    public class StoreAInfo : StoreA<StoreInfo>, IStoreInfo<StoreAExtraInfo<StoreInfo>, StoreInfo>
    {
        public string StoresString => string.Join(Environment.NewLine, AStores.Select(x => $"{x.City} {x.Name} {Extra.StoreAExtraString}"));
    }
}
