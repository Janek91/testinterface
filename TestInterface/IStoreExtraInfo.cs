﻿using System.Collections.Generic;

namespace TestInterface
{
    public interface IStoreExtraInfo<TStore>
        where TStore : Store
    {
        IList<TStore> Stores { get; set; }
    }
}
