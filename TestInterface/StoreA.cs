﻿namespace TestInterface
{
    public class StoreA<TStore> : StoreBase<StoreAExtraInfo<TStore>, TStore>
        where TStore : StoreInfo
    {
        public TStore[] AStores { get; set; }
    }
}
